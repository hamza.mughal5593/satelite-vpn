package com.satelite.vpnapp.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;
import java.util.Set;

public class Utils {

    public static void saveFave(Context context, String str) {
        SharedPreferences saveHistory = context.getSharedPreferences("PrefName",0);
        SharedPreferences.Editor editor = saveHistory.edit();
        Set<String> x = readFaves(context);
        x.add(str);
        editor.putStringSet("favorites", x);
        editor.commit();
    }

    public static void removeFave(Context context, String str) {
        SharedPreferences saveHistory = context.getSharedPreferences("PrefName",0);
        SharedPreferences.Editor editor = saveHistory.edit();
        Set<String> x = readFaves(context);
        x.remove(str);
        editor.putStringSet("favorites", x);
        editor.commit();
    }

    public static Set<String> readFaves(Context context) {
        SharedPreferences readHistory = context.getSharedPreferences("PrefName",0);
        return readHistory.getStringSet("favorites", new HashSet<String>() );
    }

    public static void clearFaves(Context context) {
        SharedPreferences saveHistory = context.getSharedPreferences("PrefName",0);
        SharedPreferences.Editor editor = saveHistory.edit();
        editor.putStringSet("favorites", new HashSet<String>());
        editor.commit();
    }

}
