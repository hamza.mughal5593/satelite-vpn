package com.satelite.vpnapp.ServerList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.satelite.vpnapp.R;
import com.satelite.vpnapp.ServerList.Models.CountryListModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class RecyclerViewAdapterSearch extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    //public class RecyclerViewAdapterSearch  {
    // A menu item view type.
    private static final int MENU_ITEM_VIEW_TYPE = 0;


    // The banner ad view type.
    private static final int BANNER_AD_VIEW_TYPE = 1;

    // An Activity's Context.
    private final Context mcontext;

    // The list of banner ads and menu items.
    private List<CountryListModel> recyclerViewItems;

    private ArrayList<CountryListModel> arraylist=new ArrayList<>();

    public RecyclerViewAdapterSearch(Context context, List<CountryListModel> recyclerViewItems, RecyclerViewAdapterSearch.OnItemClickListener clickListener) {
        this.mcontext = context;
        this.recyclerViewItems = recyclerViewItems;
        this.itemClickListener = clickListener;
        this.arraylist.addAll(recyclerViewItems);

    }

    /**
     * The {@link RecyclerViewAdapterSearch.MenuItemViewHolder} class.
     * Provides a reference to each view in the menu item view.
     */
    public class MenuItemViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        public ImageView flag_img;
        public TextView country_name;
        public TextView city_name;
        public TextView ping_val;
        public LinearLayout ll;
        public ImageButton heart;
        MenuItemViewHolder(View view) {
            super(view);
            mView = view;
            flag_img = view.findViewById(R.id.iv_flag);
            country_name = view.findViewById(R.id.tv_country);
            city_name = view.findViewById(R.id.tv_city);
            ping_val = view.findViewById(R.id.tv_ping);
            ll = view.findViewById(R.id.ll_item);
            heart = view.findViewById(R.id.heartBtn);
        }
    }



    @Override
    public int getItemCount() {
        return recyclerViewItems.size();
    }

    /**
     * Determines the view type for the given position.
     */
    @Override
    public int getItemViewType(int position) {
        return MENU_ITEM_VIEW_TYPE;
    }
    /**
     * Creates a new view for a menu item view or a banner ad view
     * based on the viewType. This method is invoked by the layout manager.
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View menuItemLayoutView = null;
        switch (viewType) {
            case MENU_ITEM_VIEW_TYPE:
                menuItemLayoutView = LayoutInflater.from(mcontext).inflate(R.layout.server_list_item, viewGroup, false);
                return new RecyclerViewAdapterSearch.MenuItemViewHolder(menuItemLayoutView);
            default:
                return new RecyclerViewAdapterSearch.MenuItemViewHolder(menuItemLayoutView);
        }
    }
    RecyclerViewAdapterSearch.OnItemClickListener itemClickListener;
    /**
     * Replaces the content in the views that make up the menu item view and the
     * banner ad view. This method is invoked by the layout manager.
     */
    @SuppressLint("ResourceType")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case MENU_ITEM_VIEW_TYPE:
                final RecyclerViewAdapterSearch.MenuItemViewHolder menuItemHolder = (RecyclerViewAdapterSearch.MenuItemViewHolder) holder;
                final CountryListModel model = (CountryListModel) recyclerViewItems.get(position);
                menuItemHolder.country_name.setText(model.GetCountry());
                menuItemHolder.city_name.setText(model.GetCity());
//                if(model.Ping != null) {
//                    int ping = model.GetPing();
//                    menuItemHolder.ping_val.setText(ping + "ms");
//                    if (ping < 80)
//                        menuItemHolder.ping_val.setTextColor(Color.WHITE);
//                    else if (ping > 79 && ping < 160)
//                        menuItemHolder.ping_val.setTextColor(mcontext.getResources().getColor(R.color.white));
//                    else
//                        menuItemHolder.ping_val.setTextColor(Color.RED);
//                }

//                SharedPreferences ConnectionDetails = mcontext.getSharedPreferences("connection_data", 0);
//                String Ip = ConnectionDetails.getString("ip", "NA");
//
//                if(Ip.equals(model.GetIP()))
//                    menuItemHolder.ll.setBackgroundResource(R.color.white);

                menuItemHolder.flag_img.setImageResource(mcontext.getResources().getIdentifier(model.GetImage(), "drawable", mcontext.getPackageName()));




                if (recyclerViewItems.get(position).isSelected()) {
                    menuItemHolder.ll.setBackgroundResource(R.color.gray_bar);

                } else {
                    menuItemHolder.ll.setBackgroundResource(Color.TRANSPARENT);
                }

                holder.itemView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {


                        if (recyclerViewItems.get(position).isSelected()){
                            menuItemHolder.ll.setBackgroundResource(R.color.gray_bar);

                        }else {
                            menuItemHolder.ll.setBackgroundResource(Color.TRANSPARENT);

                        }

                        itemClickListener.onItemClicked_search(model,position,model.Favorite);

notifyDataSetChanged();

                    }
                });
                if(model.GetFavorite())
                    menuItemHolder.heart.setImageDrawable(mcontext.getDrawable(android.R.drawable.btn_star_big_on));
                else
                    menuItemHolder.heart.setImageDrawable(mcontext.getDrawable(android.R.drawable.btn_star_big_off));

                menuItemHolder.heart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean result = itemClickListener.onHeartClicked__search(model);
                        if (result)
                            menuItemHolder.heart.setImageDrawable(mcontext.getDrawable(android.R.drawable.btn_star_big_on));
                        else
                            menuItemHolder.heart.setImageDrawable(mcontext.getDrawable(android.R.drawable.btn_star_big_off));
                    }
                });
                break;
            case BANNER_AD_VIEW_TYPE:
                break;
        }
    }
    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        recyclerViewItems.clear();
        if (charText.length() == 0) {
            recyclerViewItems.addAll(arraylist);
        } else {
            for (CountryListModel wp : arraylist) {
                if (wp.GetCountry().toLowerCase(Locale.getDefault()).contains(charText)) {
                    recyclerViewItems.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
    //    public void filterList(ArrayList<CountryListModel> filteredList) {
//        recyclerViewItems = filteredList;
//        notifyDataSetChanged();
//    }
    public interface OnItemClickListener{
        void onItemClicked_search(CountryListModel server, int postion, boolean favorite);
        boolean onHeartClicked__search(CountryListModel server);
    }
}
