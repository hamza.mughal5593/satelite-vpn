package com.satelite.vpnapp.ServerList.Models;

public class CountryListModel {
    public String ID;
    public String File;
    public String Country;
    public String City;
    public String Image;
    public String IP;
    public String Port;
    public Integer Ping;
    public boolean Favorite = false;
    public boolean isSelected = false;


    public CountryListModel(String ID, String file, String country, String city, String image, String IP, String port, Integer ping, boolean favorite) {
        this.ID = ID;
        File = file;
        Country = country;
        City = city;
        Image = image;
        this.IP = IP;
        Port = port;
        Ping = ping;
        Favorite = favorite;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String GetID() {
        return ID;
    }
    public void SetID(String ID) {
        this.ID = ID;
    }

    public String GetFileID() {
        return File;
    }
    public void SetFileID(String FileID) {
        this.File = FileID;
    }

    public String GetCountry() {
        return Country;
    }
    public void SetCountry(String Country) {
        this.Country = Country;
    }

    public String GetImage() {
        return Image;
    }
    public void SetImage(String Image) {
        this.Image = Image;
    }

    public String GetPort() { return Port; }

    public void SetPort(String port ) { this.Port = port; }

    public String GetCity() { return City; }

    public void SetCity(String city) { this.City = city; }

    public String GetIP() { return IP; }

    public void SetIP(String IP) { this.IP = IP; }

    public Integer GetPing() { return Ping; }

    public void SetPing(Integer Ping) { this.Ping = Ping; }

    public boolean GetFavorite() { return Favorite; }

    public void SetFavorite(boolean fave) { this.Favorite = fave; }

}
