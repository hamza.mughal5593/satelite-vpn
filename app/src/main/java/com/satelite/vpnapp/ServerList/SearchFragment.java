package com.satelite.vpnapp.ServerList;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.satelite.vpnapp.R;
import com.satelite.vpnapp.ServerList.Models.CountryListModel;

import java.util.ArrayList;
import java.util.List;


public class SearchFragment extends Fragment {
    LocationActivity activity;
    ArrayList<CountryListModel> search_list;

    public SearchFragment(ArrayList<CountryListModel> search_list, LocationActivity locationActivity) {
        // Required empty public constructor
        this.search_list = search_list;
        this.activity = locationActivity;
    }

    View view;
    RecyclerView childRecyclerView;
    EditText et_search_data;
    RecyclerViewAdapterSearch childRecyclerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_search, container, false);

        childRecyclerView = view.findViewById(R.id.childRecyclerView);
        et_search_data = view.findViewById(R.id.et_search_data);

        childRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        childRecyclerAdapter = new RecyclerViewAdapterSearch(getActivity(), search_list, activity);
        childRecyclerView.setAdapter(childRecyclerAdapter);


        et_search_data.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
//                filter(s.toString());

                childRecyclerAdapter.filter(s.toString());

            }
        });


        return view;
    }

//    private void filter(String text) {
//        ArrayList<CountryListModel> filteredList = new ArrayList<>();
//
//        for (CountryListModel item : filteredList) {
//            if (item.GetCountry().toLowerCase().contains(text.toLowerCase())) {
//                filteredList.add(item);
//            }
//        }
//
//        childRecyclerAdapter.filterList(filteredList);
//    }
}