package com.satelite.vpnapp.ServerList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.satelite.vpnapp.R;
import com.satelite.vpnapp.ServerList.Models.CountryListModel;
import com.satelite.vpnapp.ServerList.Models.Section;

import java.util.List;

public class MainRecyclerAdapter  extends RecyclerView.Adapter<MainRecyclerAdapter.ViewHolder>  {
    List<Section> sectionList;
    Context mcontext;
    LocationActivity lAct;
    public MainRecyclerAdapter(Activity ctx, List<Section> sectionList, LocationActivity l) {
        this.sectionList = sectionList;
        this.mcontext=ctx;
        this.lAct = l;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.section_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        Section section = sectionList.get(position);
        String sectionName = section.getSectionName();
        final List<CountryListModel> objectitems  = section.getSectionItems();

        holder.sectionNameTextView.setText(sectionName);



        holder.childRecyclerView.setLayoutManager(new LinearLayoutManager(mcontext));
        RecyclerViewAdapter childRecyclerAdapter = new RecyclerViewAdapter(mcontext,objectitems,lAct);
        holder.childRecyclerView.setAdapter(childRecyclerAdapter);
    }

    @Override
    public int getItemCount() {
        return sectionList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView sectionNameTextView;
        RecyclerView childRecyclerView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            sectionNameTextView = itemView.findViewById(R.id.sectionNameTextView);
            childRecyclerView = itemView.findViewById(R.id.childRecyclerView);
        }
    }


    @SuppressLint("WrongConstant")
    @RequiresApi(api = 19)
    private boolean isAccessGranted() {
        boolean z = false;
        try {
            ApplicationInfo applicationInfo = mcontext.getPackageManager().getApplicationInfo(mcontext.getPackageName(), 0);
            if ((Build.VERSION.SDK_INT > 19 ? ((AppOpsManager)mcontext.getSystemService("appops")).checkOpNoThrow("android:get_usage_stats", applicationInfo.uid, applicationInfo.packageName) : 0) == 0) {
                z = true;
            }
            return z;

        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }
}

