package com.satelite.vpnapp.UserAccount;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.satelite.vpnapp.MainActivity;
import com.satelite.vpnapp.R;

public class LoginActivity extends AppCompatActivity {
TextView save_btn,signup_btn,online_guide;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        save_btn= findViewById(R.id.save_btn);
        signup_btn= findViewById(R.id.signup_btn);
        online_guide= findViewById(R.id.online_guide);

        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                Intent intent= new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        signup_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        online_guide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }
}