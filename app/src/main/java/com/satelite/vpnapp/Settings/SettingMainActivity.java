package com.satelite.vpnapp.Settings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.satelite.vpnapp.R;
import com.satelite.vpnapp.ServerList.LocationActivity;
import com.satelite.vpnapp.Settings.AccountSettings.AccountSettingActivity;
import com.satelite.vpnapp.Settings.VPNSettings.VPNSettingMainActivity;
import com.satelite.vpnapp.ViewLogsActivity;

public class SettingMainActivity extends AppCompatActivity {
LinearLayout view_logs,bookmarks_btn,privacy_btn,help_btn,invite_btn,guideline_btn;
    ImageView back_btn;
    LinearLayout account_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_m_ain);

        view_logs = findViewById(R.id.view_logs);
        account_btn = findViewById(R.id.account_btn);
        bookmarks_btn = findViewById(R.id.bookmarks_btn);
        privacy_btn = findViewById(R.id.privacy_btn);
        invite_btn = findViewById(R.id.invite_btn);
        back_btn= findViewById(R.id.back_btn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        view_logs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(SettingMainActivity.this, ViewLogsActivity.class);
                startActivity(intent);
            }
        });


        bookmarks_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(SettingMainActivity.this, "Bookmark btn", Toast.LENGTH_SHORT).show();
//                Intent intent= new Intent(SettingMainActivity.this, ApplicationSetting.class);
//                startActivity(intent);
            }
        });
        account_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(SettingMainActivity.this, AccountSettingActivity.class);
                startActivity(intent);
            }
        });
        privacy_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(SettingMainActivity.this, "privacy btn", Toast.LENGTH_SHORT).show();
//                Intent intent= new Intent(SettingMainActivity.this, AboutActivity.class);
//                startActivity(intent);
            }
        });
//        help_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent= new Intent(SettingMainActivity.this, HelpMainActivity.class);
//                startActivity(intent);
//            }
//        });
        invite_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(SettingMainActivity.this, "Invite Friend", Toast.LENGTH_SHORT).show();

//                Intent intent= new Intent(SettingMainActivity.this, LocationActivity.class);
//                startActivity(intent);
            }
        });
//        guideline_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent= new Intent(SettingMainActivity.this, OnlineGuideActivity.class);
//                startActivity(intent);
//            }
//        });



    }
}