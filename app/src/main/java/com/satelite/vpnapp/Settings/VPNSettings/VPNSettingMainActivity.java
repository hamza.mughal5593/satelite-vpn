package com.satelite.vpnapp.Settings.VPNSettings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.satelite.vpnapp.R;

public class VPNSettingMainActivity extends AppCompatActivity {
RelativeLayout vpn_protocal;
    ImageView back_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_v_p_n_setting_main);

        vpn_protocal= findViewById(R.id.vpn_protocal);

        back_btn= findViewById(R.id.back_btn);


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        vpn_protocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(VPNSettingMainActivity.this,VPNProtocalActivity.class);
                startActivity(intent);
            }
        });


    }
}