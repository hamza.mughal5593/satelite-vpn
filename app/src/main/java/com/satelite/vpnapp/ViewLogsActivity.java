package com.satelite.vpnapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.satelite.vpnapp.Settings.SettingMainActivity;

public class ViewLogsActivity extends AppCompatActivity {
ImageView setting_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_logs);

        setting_btn= findViewById(R.id.setting_btn);

        setting_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViewLogsActivity.this, SettingMainActivity.class);
                startActivity(intent);
            }
        });
    }
}