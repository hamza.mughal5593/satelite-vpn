package com.satelite.vpnapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.satelite.vpnapp.ServerList.LocationActivity;
import com.satelite.vpnapp.Settings.SettingMainActivity;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {
    ImageView setting_btn;
    RelativeLayout location_btn;
    TextView view_logs;
CircleImageView  country_image_circle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        setting_btn = findViewById(R.id.setting_btn);
        location_btn = findViewById(R.id.location_btn);
        view_logs = findViewById(R.id.view_logs);
        country_image_circle = findViewById(R.id.country_image_circle);
//
        setting_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SettingMainActivity.class);
                startActivity(intent);
            }
        });
        location_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, LocationActivity.class);
                startActivity(intent);
            }
        });
        view_logs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, ViewLogsActivity.class);
                startActivity(intent);


            }
        });


//        throw new RuntimeException("aj crash aya hai date 11 5");

        final Bitmap toBeCropped = ((BitmapDrawable)country_image_circle.getDrawable()).getBitmap();
        final BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inTargetDensity = 1;
        toBeCropped.setDensity(Bitmap.DENSITY_NONE);
        Bitmap croppedBitmap = Bitmap.createBitmap(toBeCropped, 2, 14, 60, 38);
        country_image_circle.setImageBitmap(croppedBitmap);
    }


}